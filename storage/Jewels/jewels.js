let jewelsdb = {
    "table": [{
            "name": "Vintage Pascal Original Necklace",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/necklace/1.png"
        },
        {
            "name": "Vintage Pascal Original Necklace",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/necklace/1.png"
        },
        {
            "name": "Vintage Pascal Original Necklace",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/necklace/1.png"
        },
        {
            "name": "Modern Pascal Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "The Pascal Originals",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/necklace/2.png"
        },
        {
            "name": "Modern Pascal Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "The Pascal Originals",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/necklace/2.png"
        },
        {
            "name": "Modern Pascal Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "The Pascal Originals",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/necklace/2.png"
        },
        {
            "name": "Vintage Pascal Original Ear-ring",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "ear-rings",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/earring/1.webp"
        },
        {
            "name": "Vintage Pascal Original Ear-ring",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "ear-rings",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/earring/1.webp"
        },
        {
            "name": "Vintage Pascal Original Ear-ring",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "ear-rings",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/earring/1.webp"
        },
        {
            "name": "Vintage Pascal Original Braelets",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "bracelets",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/bracelet/1.webp"
        },
        {
            "name": "Vintage Pascal Original Braelets",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "bracelets",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/bracelet/1.webp"
        },
        {
            "name": "Vintage Pascal Original Braelets",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "bracelets",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/bracelet/1.webp"
        },
        {
            "name": "Vintage Pascal Original Watch",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "watch",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/watch/1.webp"
        },
        {
            "name": "Vintage Pascal Original Watch",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "watch",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/watch/1.webp"
        },
        {
            "name": "Vintage Pascal Original Watch",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "watch",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/watch/1.webp"
        },
        {
            "name": "Vintage Pascal Original Ring",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "ring",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/ring/1.webp"
        },
        {
            "name": "Vintage Pascal Original Ring",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "ring",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/ring/1.webp"
        },
        {
            "name": "Vintage Pascal Original Ring",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "ring",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/ring/1.webp"
        },
        {
            "name": "Vintage Pascal Original Cufflinks",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "cufflinks",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/cufflinks/1.png"
        },
        {
            "name": "Vintage Pascal Original Cufflinks",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "cufflinks",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/cufflinks/1.png"
        },
        {
            "name": "Vintage Pascal Original Cufflinks",
            "description": "Red ruby flower touch",
            "brand": "The Pascal Originals",
            "type": "cufflinks",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/pascal/cufflinks/1.png"
        },





        {
            "name": "Modern Mystic Necklace",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern Mystic Necklace",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern Mystic Necklace",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern Mystic Original Necklace",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern Mystic Original Necklace",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern mystic Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "Mystic",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern Mystic Ear-ring",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "ear-rings",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern Mystic Ear-ring",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "ear-rings",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern Mystic Ear-ring",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "ear-rings",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern Mystic Braelets",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "bracelets",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern Mystic Braelets",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "bracelets",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern Mystic Braelets",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "bracelets",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern Mystic Watch",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "watch",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern Mystic Watch",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "watch",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern Mystic Watch",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "watch",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern Mystic Ring",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "ring",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern Mystic Ring",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "ring",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern Mystic Ring",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "ring",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern Mystic Cufflinks",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "cufflinks",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern Mystic Cufflinks",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "cufflinks",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern Mystic Cufflinks",
            "description": "Swift material paint touch",
            "brand": "Mystic",
            "type": "cufflinks",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },





        {
            "name": "Modern panorama Necklace",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern panorama Necklace",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern panorama Necklace",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern panorama Original Necklace",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern panorama Original Necklace",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern panorama Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "panorama",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern panorama Ear-ring",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "ear-rings",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern panorama Ear-ring",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "ear-rings",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern panorama Ear-ring",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "ear-rings",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern panorama Braelets",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "bracelets",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern panorama Braelets",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "bracelets",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern panorama Braelets",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "bracelets",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern panorama Watch",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "watch",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern panorama Watch",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "watch",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern panorama Watch",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "watch",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern panorama Ring",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "ring",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern panorama Ring",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "ring",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern panorama Ring",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "ring",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern panorama Cufflinks",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "cufflinks",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern panorama Cufflinks",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "cufflinks",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern panorama Cufflinks",
            "description": "Swift material paint touch",
            "brand": "panorama",
            "type": "cufflinks",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },



        {
            "name": "Modern light Necklace",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern light Necklace",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern light Necklace",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern light Original Necklace",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern light Original Necklace",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern light Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "light",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern light Ear-ring",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "ear-rings",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern light Ear-ring",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "ear-rings",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern light Ear-ring",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "ear-rings",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern light Braelets",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "bracelets",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern light Braelets",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "bracelets",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern light Braelets",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "bracelets",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern light Watch",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "watch",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern light Watch",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "watch",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern light Watch",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "watch",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern light Ring",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "ring",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern light Ring",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "ring",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern light Ring",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "ring",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern light Cufflinks",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "cufflinks",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern light Cufflinks",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "cufflinks",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern light Cufflinks",
            "description": "Swift material paint touch",
            "brand": "light",
            "type": "cufflinks",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },





        {
            "name": "Modern classics Necklace",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern classics Necklace",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern classics Necklace",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern classics Original Necklace",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern classics Original Necklace",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern classics Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "classics",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern classics Ear-ring",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "ear-rings",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern classics Ear-ring",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "ear-rings",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern classics Ear-ring",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "ear-rings",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern classics Braelets",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "bracelets",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern classics Braelets",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "bracelets",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern classics Braelets",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "bracelets",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern classics Watch",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "watch",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern classics Watch",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "watch",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern classics Watch",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "watch",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern classics Ring",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "ring",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern classics Ring",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "ring",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern classics Ring",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "ring",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern classics Cufflinks",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "cufflinks",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern classics Cufflinks",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "cufflinks",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern classics Cufflinks",
            "description": "Swift material paint touch",
            "brand": "classics",
            "type": "cufflinks",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },






        {
            "name": "Modern couple's garden Necklace",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern couple's garden Necklace",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern couple's garden Necklace",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/1.webp"
        },
        {
            "name": "Modern couple's garden Original Necklace",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "Necklace",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern couple's garden Original Necklace",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "Necklace",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern couple's garden Original Necklace",
            "description": "Gold touch with diamond accent",
            "brand": "couple's garden",
            "type": "Necklace",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/necklace/2.webp"
        },
        {
            "name": "Modern couple's garden Ear-ring",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "ear-rings",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern couple's garden Ear-ring",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "ear-rings",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern couple's garden Ear-ring",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "ear-rings",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/earring/1.webp"
        },
        {
            "name": "Modern couple's garden Braelets",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "bracelets",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern couple's garden Braelets",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "bracelets",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern couple's garden Braelets",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "bracelets",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/bracelet/1.webp"
        },
        {
            "name": "Modern couple's garden Watch",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "watch",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern couple's garden Watch",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "watch",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern couple's garden Watch",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "watch",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/watch/1.png"
        },
        {
            "name": "Modern couple's garden Ring",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "ring",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern couple's garden Ring",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "ring",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern couple's garden Ring",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "ring",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/ring/1.webp"
        },
        {
            "name": "Modern couple's garden Cufflinks",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "cufflinks",
            "material": "Original",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern couple's garden Cufflinks",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "cufflinks",
            "material": "Artificial",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        },
        {
            "name": "Modern couple's garden Cufflinks",
            "description": "Swift material paint touch",
            "brand": "couple's garden",
            "type": "cufflinks",
            "material": "Semi-precious",
            "price": "55,000 ₦",
            "url": "./images/semi/mystic/cufflinks/1.png"
        }
    ]
}