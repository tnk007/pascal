window.memory = [];
$(document).ready(function() {
    let jewel = jewelsdb;
    //let url = window.location.href;

    // creates a view from the model and binds it to the div that will be vieewing the model in the view
    //  this uses the create element functionality to create elements according to the size of the simulated db
    let binder = document.getElementById('cart-content');
    for (let i = 0; i < jewel.table.length; i++) {
        let cartItem = document.createElement('div'); 
        cartItem.className = "col-10 col-md-2 cart-item"
        cartItem.setAttribute("data-name", jewel.table[i].name);
        cartItem.setAttribute("data-price", jewel.table[i].price);
        cartItem.setAttribute("data-type", jewel.table[i].type);
        cartItem.setAttribute("data-material", jewel.table[i].material);
        cartItem.setAttribute("data-brand", jewel.table[i].brand);
        cartItem.setAttribute("data-id", i);
        cartItem.innerHTML = `
            <img class="menuContentItem" src="` + jewel.table[i].url + `"></img>
            <div class="cart-item-info" id="` + i + `item" data-show="false" style="display:none;">
            <div class="menuContentItemDescription"><strong>` + jewel.table[i].name + `</strong></div>
            <div class="menuContentItemDescription" style="font-style: italic; font-variant: all-petite-caps;">` + jewel.table[i].description + `</div>
            <!--<div class="menuContentItemDescription">` + jewel.table[i].price + `</div>-->
            <div class="menuBottomContentItemDescription gallerycontent like"><i class="fa fa-heart"></i></div>
            <div class="menuBottomContentItemDescription gallerycontent expand"><i class="fa fa-expand"></i></div>
            </div>
        `
        // this toggles the info displayed when one of the products in the gallery is clicked on
        // uses an attribute called data-show to toggle visibility
        cartItem.addEventListener('mouseover', (e) => {
            let all = document.getElementsByClassName('cart-item-info');
            for (let i of all) {
                i.style.display = "none";
            }
            let clicked = e.currentTarget;
            let id = clicked.getAttribute("data-id");
            let s = document.getElementById(id + "item");
            if (s.getAttribute("data-show") == "false") {
                s.style.display = "block";
                s.setAttribute("data-show", "true");
            }
        })
        cartItem.addEventListener('mouseout', (e) => {
            let all = document.getElementsByClassName('cart-item-info');
            for (let i of all) {
                i.style.display = "none";
            }
            let clicked = e.currentTarget;
            let id = clicked.getAttribute("data-id");
            let s = document.getElementById(id + "item");
            if (s.getAttribute("data-show") == "true") {
                s.style.display = "none";
                s.setAttribute("data-show", "false");
            }
        })
        binder.appendChild(cartItem);
    }

    //this is the like system that  uses the localStorage to save the products liked by the user
    //it adds the product id to a global array (a property of 'windows') which is then converted to a string and stored in the localStorage
    /*when the page is reloaded or loaded again the value is extracted from the localStorage and converted 
    to an array and is used to return the saved state back to the program*/
    let cont  = $('.like');
    let saved = window.localStorage.getItem('likes') || null;
    console.log(saved);
    if(saved != null)
    {
        saved = saved.split(",");
    for(let i=0; i < saved.length; i++){
        $("[data-id="+saved[i]+"]").children()[1].children[2].style.color = "red";
    }
}
else{
    saved = [];
}
    window.memory = saved;
    console.log(window.memory);
    $('.like').click(e=>{
        let id = e.currentTarget.parentNode.parentNode.getAttribute("data-id");
        let check= (idd)=>{
            let tmp = false;
            for(let i = 0; i <window.memory.length; i++)
            {
                if(window.memory[i] == idd){
                    tmp = true;
                }
            }
            return tmp;
        }
        let t = check(id);
        console.log(t);
        if(window.memory.length <= 0){
            window.memory.push(id);
            e.currentTarget.style.color = "red";
        }else{
            if(t == false){
                window.memory.push(id)
                e.currentTarget.style.color = "red";
            }
            else{
                e.currentTarget.style.color = "white";
                var filtered = window.memory.filter(function(value, index, arr){

                    return value != id;
                
                });
                window.memory = filtered
            }
        }
        let saveString = window.memory.toString();
        window.localStorage.setItem('likes', saveString);
        console.log(window.memory)
    })
    
    //this is the expand system it reads a particular product and expands it for more information
$('.expand').click(e=>{
    let id = e.currentTarget.parentNode.parentNode.getAttribute("data-id");
    console.log(id);
    let img = $('#expand-image');
    img[0].src = jewel.table[id].url.replace("<#>", id);
    console.log(img[0].src)
    let expandContainer = $('.expand-info');
    expandContainer.show();
    let expandDescription = $('#expand-description');
    let dContent = document.createElement('div');
    dContent.style="width:70%; font-size:2vw;";
    dContent.innerHTML=` 
    <div style="
    padding: 5%;
    background-color: mintcream;
">
<div style="display: flex;
justify-content: space-between;"> 
<div class="menuContentItemDescription"><strong>Product Name</strong></div>
<div class="menuContentItemDescription"><strong>` + jewel.table[id].name + `</strong></div>
</div>
<div style="display: flex;
justify-content: space-between;"> 
<div class="menuContentItemDescription"><strong>Product Description</strong></div>
    <div class="menuContentItemDescription" style="font-style: italic; font-variant: all-petite-caps;">` + jewel.table[id].description + `</div>
    </div>
    <div style="display: flex;
justify-content: space-between;"> 
<div class="menuContentItemDescription"><strong>Product Material</strong></div>
    <div class="menuContentItemDescription" style="font-style: italic; font-variant: all-petite-caps;">` + jewel.table[id].material + `</div>
    </div>
    </div>
    `
    expandDescription[0].innerHTML = '';
    expandDescription[0].appendChild(dContent);
});
$('.close-expand').click(e=>{
    let expandContainer = $('.expand-info');
    expandContainer.hide();
})
});

