let code = `<section class="row carouselContainer mb-3">
<div class="col-12 col-md-12 carousel">
  <div class="d-none d-md-flex row safe"><video id="cover" autoplay=true muted loop src="./videos/pascal2.mp4" class="desktop d-none d-md-flex col-12">
  </video></div>
  <div class="d-md-none d-flex"><video id="cover" autoplay=true muted loop src="./videos/pascalm.mp4" class="mobile d-md-none d-flex col-12">
  </video></div>
  <div class="col-12 col-md-4 t1">
    <div class="row">
      <div class="info col-12 tips1">
        <div><p>Angels are like diamonds. They can’t be made. You have to find them. Each one is unique.</p></div>
        </div>
        <div class="info col-12 tips1">
          <div><p>I love jewelry - gold and diamonds. I'm a human.</p></div>
          </div>
          <div class="info col-12 tips1">
            <div><p>Cheap jewelry, however, is worse than no jewelry at all, and there are very few things in life than are worse than no jewelry at all.</p></div>
            </div>
            <div class="info col-12 tips1">
              <div>
                <p>Happy New Year!<br/>Pascal Imitation Jewelry wishes you a dazzling 2020</p>
              </div>
            </div>
            <div class="info col-12 tips1">
              <div>
                <p>Big people need big diamonds, really big ones.</p>
              </div>
              </div>
              <div class="info col-12 tips1">
              <div>
                <p>Jewelry takes people's minds off your wrinkles</p>
              </div>
            </div>
    </div>
  </div>
</div>
</section>`

class Carousel extends HTMLElement {
    constructor() {
        // Always call parent constructor first
        super();
        // Insert code into elements innerHTML
        this.innerHTML = code;
    }
}
// Register new element using kebab-casing for name
customElements.define("carousel-content", Carousel);