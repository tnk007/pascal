class Footer extends HTMLElement {
    constructor() {
        // Always call parent constructor first
        super();

        // Insert code into element's innerHTML
        this.innerHTML = `<footer class="row pfooter">
        <ul class="col-12 mb-5">
          <a href="index.html"><div><h2>Pascal Imitation Jewelry</h2></div></a>
        </ul>
        <div class="col-12 mb-5 footerborder">
          <div class="row"> 
            <div class="col-12 col-md-6">
              <i class="fa fa-phone"> <span style="color:black;
              font-family: 'Cantarell', sans-serif !important;
              font-size: 2vh !important;"> +2348053471002</span></i>
            </div>
            <div class="col-12 col-md-6">
              <i class="fa fa-at"> <span style="color:black;
              font-family: 'Cantarell', sans-serif !important;
              font-size: 2vh !important;"> contact@pascal.com</span></i>
            </div>
          </div>
        </div>
        <div class="col-12 mb-5 footerborder">
          <div class="row">
            <div class="col-3">
              <i class="fab fa-facebook-f"></i>
            </div>
            <div class="col-3">
              <i class="fab fa-twitter"></i>
            </div>
            <div class="col-3">
              <i class="fab fa-google"></i>
            </div>
            <div class="col-3">
              <i class="fab fa-snapchat"></i>
            </div>
          </div>
        </div>
        <div class="col-12 mb-5">
          <div class="row footerborder">
            <div class="col-6">
              <img class="w-t30" src="images/middle.jpg" />
            </div>
            <div class="col-6">
              <img class="w-t30" src="images/aptech.png" />          
            </div>
        </div>
        <div class="col-12 mb-5 tiny">
          <div class="row">
            <div class="col-12 col-md-3 mt-3">
              <span>©Pascal Imitation Jewelry-2020 All Rights Reserved</span>
            </div>
            <div class="col-4 col-md-3 mt-3">
                <span>Privacy Policy</span>
            </div>
            <div class="col-4 col-md-3 mt-3">
              <span>Conditions of sale</span>
          </div>
          <div class="col-4 col-md-3 mt-3">
            <span>Legal</span>
        </div>
        </div>
      </footer>`;
       
    }
}
// Register new element using kebab-casing for name
customElements.define("footer-bar", Footer);