class ShoppingCart extends HTMLElement {
    constructor() {
        // Always call parent constructor first
        super();
        let code = `
        <div class="row expand-info justify-center" data-show="hide" style="display:none; width: 100%; height: 100%; position: fixed; top: 0px; z-index: 9999999; background-color: rgba(0, 0, 0, 0.5); padding: 5%;">
    <div class="col-12">
    <div class="row close-expand"><i class="fa fa-window-close"></i></div>
    <div class="justify-center" style="height:31%;"><img style="
    object-fit: contain;
    background: mintcream;
    height: 29vh;
    width: 353px;
    " id="expand-image" src="./images/semi/0.webp"></div>
    <div class="row justify-center" id="expand-description">
    
    </div>
    </div>
    </div>
        <div style="background-color: lightgrey;" class="row shoppingCart">
<div class="col-12 col-md-2 cart-control">
    <div class="row d-none d-md-flex filterbtn"><label class="btn">Filter</label></div>
    <div class="row d-flex d-md-none filterbtn"><div class="col-12"><i class="fa fa-filter">Filter</i></div></div>
    <div class="row filter d-none d-md-flex">
    <label class="col-12">Brands:</label>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-brand-checkbox checkbox-controls" type ="checkbox" value="Panorama" data-clicked="false" id="panorama" name="brand"/><label> The Panorama</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-brand-checkbox checkbox-controls" type ="checkbox" value="Mystic" data-clicked="false" id="mystic" name="brand"/><label> Mystic</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-brand-checkbox checkbox-controls" type ="checkbox" value="Classics" data-clicked="false" id="classics" name="brand"/><label> Classics</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-brand-checkbox checkbox-controls" type ="checkbox" value="Light" data-clicked="false" id="light" name="brand"/><label> Light</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-brand-checkbox checkbox-controls" type ="checkbox" value="The Pascal Originals" data-clicked="false" id="pascal" name="brand"/><label> The Pascal Originals</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-brand-checkbox checkbox-controls" type ="checkbox" value="Couple's Garden" data-clicked="false" id="couple" name="brand"/><label> The Couple's Garden</label>
        </div>
        <label class="col-12">Material:</label>
         <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Original" data-clicked="false" id="original" name="material"/><label>Original</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Semi-precious" data-clicked="false" id="semi-precious" name="material"/><label>Semi-precious</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Artificial" data-clicked="false" id="artificial" name="material"/><label>Artificial</label>
        </div>
        <label class="col-12">Type:</label>
         <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Necklace" data-clicked="false" id="necklace" name="type"/><label>Necklace</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Watch" data-clicked="false" id="watch" name="type"/><label>Watch</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Ear-rings" data-clicked="false" id="ear-ring" name="type"/><label>Ear-rings</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Cufflinks" data-clicked="false" id="cufflinks" name="type"/><label>Cufflinks</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Bracelets" data-clicked="false" id="bracelet" name="type"/><label>Bracelets</label>
        </div>
        <div class="col-12 cart-control-item"> 
            <input class="cart-control-item-material-checkbox checkbox-controls" type ="checkbox" value="Ring" data-clicked="false" id="ring" name="type"/><label>Rings</label>
        </div>
    </div>
</div>
<div class="col-12 col-md-10 cart-container">
    <div class="row cart-content" id="cart-content">
        
    </div>
</div>
</div>`;
// Insert code into element's innerHTML
        this.innerHTML = code;
    }
}
// Register new element using kebab-casing for name
customElements.define("shopping-cart", ShoppingCart);