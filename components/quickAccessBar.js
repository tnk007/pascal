class QuickAccessBar extends HTMLElement {
    constructor() {
        // Always call parent constructor first
        super();
        // Insert code into element's innerHTML
        this.innerHTML = `
        <nav class="row">
        <a href="feedback.html"><div class="feedback">Feedback</div></a>
        <ul class = "col-12 order-sm-1 order-md-3">
            <a href="index.html"><div><h1>Pascal Imitation Jewelry</h1></div></a>
          </ul>
        <ul class="col-12 col-md-6 order-md-2 quickAccessItemContainer">
        <div class="row">
          <ul class="col-2 order-sm-3 d-md-none" id="menu">
          <div class="row">
            <li class="col-12"><i class="fa fa-bars"></i></li>
            </div>
          </ul>
          <ul class="col-10 order-sm-3" id="search">
          <div class="row">
              <li class="col-12"><div class="row" style="justify-content: flex-end;
              padding-right: 8%;align-items: baseline;"><i class="col-2 fa fa-search" id="searchBtn"></i><input type="search" class="col-10 searchBar" placeholder="Search"/></div>
              </li>
              </div>
          </ul>
          </div>
          </ul>
          <ul class="col-sm-4 col-md-12 order-sm-4 d-none d-md-block" id="brand">
          <div class="row" style="justify-content: center;">
            <li class="col-sm-12 col-md-2"><a class="btn menuBarItem" id="1" name="1m">Pascal Originals</a>
              <div class="dropdown"></div>
            </li>
            <li class="col-sm-12 col-md-2"><a class="btn menuBarItem" id="2" name="2m">Mystic</a>
              <div class="dropdown"></div>
            </li>
            <li class="col-sm-12 col-md-2"><a class="btn menuBarItem" id="3" name="3m">Light</a>
              <div class="dropdown"></div>
            </li>
            <li class="col-sm-12 col-md-2"><a class="btn menuBarItem" id="4" name="4m">Panorama</a>
              <div class="dropdown"></div>
            </li>
            <li class="col-sm-12 col-md-2"><a class="btn menuBarItem" id="5" name="5m">Couple's Garden</a>
              <div class="dropdown"></div>
            </li>
            <li class="col-sm-12 col-md-2"><a class="btn menuBarItem" id="6" name="6m">Classics</a>
            <div class="dropdown"></div>
          </li>
        </li>
            <div class="topContent mt-2">
          <div class="row menuContentContainer" id="1m" name="hide">
          <a class="col-3" href="gallery.html?necklace"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/necklace/1.png"/><span class="d-block">Necklace</span></div></a>
          <a class="col-3" href="gallery.html?cufflinks"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/cufflinks/1.png"/><span class="d-block">Cufflinks</span></div></a>
          <a class="col-3" href="gallery.html?ear-ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/earring/1.webp"/><span class="d-block">Ear-rings</span></div></a>
          <a class="col-3" href="gallery.html?ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/ring/1.webp"/><span class="d-block">Rings</span></div></a>
          <a class="col-3" href="gallery.html?bracelets"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/bracelet/1.webp"/><span class="d-block">Bracelets</span></div></a>
          <a class="col-3" href="gallery.html?watch"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/watch/1.webp"/><span class="d-block">Watches</span></div></a>
          </div>
          <div class="row menuContentContainer" id="2m" name="hide">
            <a class="col-3" href="gallery.html?necklace"><div><img class="menuContentItem img-responsive" src="./images/semi/mystic/necklace/1.webp"/><span class="d-block">Necklace</span></div></a>
            <a class="col-3" href="gallery.html?cufflinks"><div><img class="menuContentItem img-responsive" src="./images/semi/mystic/cufflinks/1.png"/><span class="d-block">Cufflinks</span></div></a>
    <a class="col-3" href="gallery.html?ear-ring"><div><img class="menuContentItem img-responsive" src="./images/semi/mystic/earring/1.webp"/><span class="d-block">Ear-rings</span></div></a>
            <a class="col-3" href="gallery.html?ring"><div><img class="menuContentItem img-responsive" src="./images/semi/mystic/ring/1.webp"/><span class="d-block">Rings</span></div></a>
            <a class="col-3" href="gallery.html?bracelets"><div><img class="menuContentItem img-responsive" src="./images/semi/mystic/bracelet/1.webp"/><span class="d-block">Bracelets</span></div></a>
            <a class="col-3" href="gallery.html?watch"><div><img class="menuContentItem img-responsive" src="./images/semi/mystic/watch/1.png"/><span class="d-block">Watches</span></div></a>
          </div>
          <div class="row menuContentContainer" id="3m" name="hide">
            <a class="col-3" href="gallery.html?necklace"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/necklace/1.png"/><span class="d-block">Necklace</span></div></a>
          <a class="col-3" href="gallery.html?cufflinks"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/cufflinks/1.png"/><span class="d-block">Cufflinks</span></div></a>
          <a class="col-3" href="gallery.html?ear-ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/earring/1.webp"/><span class="d-block">Ear-rings</span></div></a>
          <a class="col-3" href="gallery.html?ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/ring/1.webp"/><span class="d-block">Rings</span></div></a>
          <a class="col-3" href="gallery.html?bracelets"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/bracelet/1.webp"/><span class="d-block">Bracelets</span></div></a>
          <a class="col-3" href="gallery.html?watch"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/watch/1.webp"/><span class="d-block">Watches</span></div></a>
          </div>
          <div class="row menuContentContainer" id="4m" name="hide">
            <a class="col-3" href="gallery.html?necklace"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/necklace/1.png"/><span class="d-block">Necklace</span></div></a>
          <a class="col-3" href="gallery.html?cufflinks"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/cufflinks/1.png"/><span class="d-block">Cufflinks</span></div></a>
          <a class="col-3" href="gallery.html?ear-ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/earring/1.webp"/><span class="d-block">Ear-rings</span></div></a>
          <a class="col-3" href="gallery.html?ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/ring/1.webp"/><span class="d-block">Rings</span></div></a>
          <a class="col-3" href="gallery.html?bracelets"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/bracelet/1.webp"/><span class="d-block">Bracelets</span></div></a>
          <a class="col-3" href="gallery.html?watch"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/watch/1.webp"/><span class="d-block">Watches</span></div></a>
          </div>
          <div class="row menuContentContainer" id="5m" name="hide">
            <a class="col-3" href="gallery.html?necklace"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/necklace/1.png"/><span class="d-block">Necklace</span></div></a>
          <a class="col-3" href="gallery.html?cufflinks"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/cufflinks/1.png"/><span class="d-block">Cufflinks</span></div></a>
          <a class="col-3" href="gallery.html?ear-ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/earring/1.webp"/><span class="d-block">Ear-rings</span></div></a>
          <a class="col-3" href="gallery.html?ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/ring/1.webp"/><span class="d-block">Rings</span></div></a>
          <a class="col-3" href="gallery.html?bracelets"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/bracelet/1.webp"/><span class="d-block">Bracelets</span></div></a>
          <a class="col-3" href="gallery.html?watch"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/watch/1.webp"/><span class="d-block">Watches</span></div></a>
          </div>
          <div class="row menuContentContainer" id="6m" name="hide">
            <a class="col-3" href="gallery.html?necklace"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/necklace/1.png"/><span class="d-block">Necklace</span></div></a>
          <a class="col-3" href="gallery.html?cufflinks"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/cufflinks/1.png"/><span class="d-block">Cufflinks</span></div></a>
          <a class="col-3" href="gallery.html?ear-ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/earring/1.webp"/><span class="d-block">Ear-rings</span></div></a>
          <a class="col-3" href="gallery.html?ring"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/ring/1.webp"/><span class="d-block">Rings</span></div></a>
          <a class="col-3" href="gallery.html?bracelets"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/bracelet/1.webp"/><span class="d-block">Bracelets</span></div></a>
          <a class="col-3" href="gallery.html?watch"><div><img class="menuContentItem img-responsive" src="./images/semi/pascal/watch/1.webp"/><span class="d-block">Watches</span></div></a>
          </div>
        </div>
            </div>
          </ul>
          <ul class="col-sm-4 col-md-6 order-sm-2 order-md-1 d-none d-md-block" id="qab">
            <div class="row justify-center">
              <a href="contactus.html"><li class="col-4"><button class="btn"><i class="fa fa-phone"></i><span class="hidden"> Contact</span></button></li></a>
              <a href="gallery.html"><li class="col-4"><button class="btn"><i class="fa fa-map-marker"></i><span class="hidden"> Gallery</span></button></li>
              <a href="brands.html"><li class="col-4"><button class="btn"><i class="fa fa-shopping-bag"></i><span class="hidden"> Brands</span></button></li>
            </div>
          </ul>
        </nav>
        `;
    }
}
// Register new element using kebab-casing for name
customElements.define("quick-access-bar", QuickAccessBar);