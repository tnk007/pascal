$(document).ready(function() {
    let shown = (name, brand)=>{
        brand.map((m)=>{
            brand[m].setAttribute("data-show", "hide");
            brand[m].style.display="none";
            if(brand[m].id == name){
                brand[m].setAttribute("data-show", "show");
                $(brand[m]).fadeIn(1000);
            }
        })
    };
    $('[data-show="hide"]').hide();
    let brand = $('.brand-info');
    let list = $('.list');
    list.click((e)=>{
        let name = e.currentTarget.getAttribute("name");
        console.log(name);
        shown(name, brand);
        
    })

});