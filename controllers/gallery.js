/*
This is the query engine used in filtering the gallery according to specified parameters
*/

let query = (checked, cartitems) => {
    let filteredCart = []
    /* the variable with the logic for reading through all the products, in it you will see a 
    <expression> that has been placed there whwich will be later replaced with a dynamically created expression*/
    let str = `
    filteredCart = [];
    for(let i = 0; i < cartitems.length; i++)
    {
    if(<expression>){
        filteredCart.push(cartitems[i]);
    }
    }
    `
    let i = 0
    let exp = ""
    //the dynamic expression is created by mapping through the selected queries and creating a comparison expression
    checked.map(c => {

        exp += `cartitems[i].getAttribute("data-` + c.name + `").toLowerCase()=="` + c.value.toLowerCase() + `" || cartitems[i].getAttribute("data-` + c.name + `").toLowerCase()=="` + c.value.toLowerCase() + `" ||`;
    })
    if (exp == "") {
        str = "";
        filteredCart = [];
    }
    console.log(exp);
    exp = exp.slice(0, -3);
    str = str.replace("<expression>", exp); // the expression symbol is replaced by the real expression
    console.log(str);
    // the str varaible is then fed into the eval() for the compiler to evaluate
    eval(str);
    return filteredCart;// the new list of products is then returned
}
let checked = []
$(document).ready(function() {
    let url = window.location.href;
    let params = url.split('?')[1];
    if (params == null)
        console.log(params);
    else{
        setTimeout(()=>{
            $('#'+params)[0].click();
        }, 1000)
    }
    setTimeout(()=>{
        $('.cart-item').slideDown(1000);
    }, 500)
    $('.filterbtn').click(e => {
        $('.filter').toggleClass('d-none', 'd-flex');
    })
    $('.checkbox-controls').click(e => {
        let filteredCartItems = [];
        let clicked = e.currentTarget;
        let content = $('.cart-item');
        if (clicked.getAttribute("data-clicked") == "false") {
            clicked.setAttribute("data-clicked", "true")
            checked.push(clicked);
            filteredCartItems = query(checked, content);
            console.log(checked);
            content.hide();
            console.log(filteredCartItems);
            filteredCartItems.map(cartitems => {
                $(cartitems).show();
            })
        } else {
            let filtered = checked.filter((value, index, arr) => {
                return value.id != clicked.id
            })
            checked = filtered;
            filteredCartItems = query(filtered, content);
            clicked.setAttribute("data-clicked", "false");
            content.hide();
            filteredCartItems.map(cartitems => {
                $(cartitems).show();
            })
            if (filteredCartItems.length <= 0)
                content.show();
            console.log(checked)
        }
    })
});