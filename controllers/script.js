/*
this controls the tips slideshow and uses timeout to slide through the divs with the content
every 5 seconds

We are utilizing recursion to simulate a loop that the slideshow uses
*/
$(document).ready(function() {

        let slideshow = ()=>{
            let tips1 = $('.tips1');
        tips1.hide();
        $(tips1[0]).show();
        let i = 1;
        let j = 0;
        let self = ()=>{window.setTimeout(()=>{
            console.log("sadds");
            if(j >= 0){
                $(tips1[j]).slideUp();
            }
            $(tips1[i]).slideDown();
            j = i;
            if(i == tips1.length-1)
            {
                i = -1;
                j = tips1.length-1
            }
            i++;
            self(); //recursive function call because it calls itself
        }, 5000);}
        self();
        };
        slideshow();
    });