$(document).ready(function() {
    setTimeout(()=>{
        $('#root').fadeIn(1000);
    }, 100)
    //this code adds the toggle behaviour of the search UI
    //quickAccessBar
    $('#searchBtn').click(() => {
        let searchBar = $('.searchBar');
        searchBar[0].style.backgroundColor = "rgb(204, 204, 204)";
        searchBar.toggle();
    })

    $('#menu').click(() => {
        $('#qab').toggleClass("d-none");
        $('#brand').toggleClass("d-none");
    })

    let menuContainer = $('.menuContentContainer');
    $('.topContent').hide();
    $('.menuBarItem').click(e => {
            $('.topContent').show();
            menuContainer.hide();
            let clicked = document.getElementById(e.currentTarget.id);
            let clickedContent = document.getElementById(clicked.name);
            console.log(clickedContent.getAttribute("name"))
            if (clickedContent.getAttribute("name") == "show") {
                menuContainer.hide();
                for (let i of menuContainer) {
                    i.setAttribute("name", "hide");
                }
                clickedContent.setAttribute("name", "hide");
                $('.topContent')[0].style.borderTop = "solid 0px lightgrey"
            } else {
                for (let i of menuContainer) {
                    i.setAttribute("name", "hide");
                }
                $(clickedContent).show();
                clickedContent.setAttribute("name", "show");
                $('.topContent')[0].style.borderTop = "solid 1px lightgrey"
            }
        })
        //quickAccessBar
});